#!/bin/bash

if [[ $1 == archlinux ]] || [[ $1 == existing ]] || [[ $1 == select ]] || [[ $1 == default ]]
then
	if [[ $1 == archlinux ]]
	then
		mkdir --verbose --parents /etc/kernels
		curl --retry 3 --retry-delay 5 https://raw.githubusercontent.com/archlinux/svntogit-packages/packages/linux-lts/trunk/config --output /etc/kernels/kernel-config-archlinux-lts
		sed -i s/\#\ CONFIG\_MODULE\_COMPRESS\_GZIP\ is\ not\ set/CONFIG\_MODULE\_COMPRESS\_GZIP\=y/g /etc/kernels/kernel-config-archlinux-lts
		sed -i s/CONFIG\_MODULE\_COMPRESS\_XZ\=y/\#\ CONFIG\_MODULE\_COMPRESS\_XZ\ is\ not\ set/g /etc/kernels/kernel-config-archlinux-lts
		if [[ -e /usr/src/linux/.config  ]]; then mv --verbose --force /usr/src/linux/.config /usr/src/linux/.config--$(date +%Y-%m-%d--%H-%M-%S).bak; fi
		genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --mrproper --no-splash --no-ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/etc/kernels/kernel-config-archlinux-lts all && genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --no-clean --no-mrproper --no-splash --ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/usr/src/linux/.config --initramfs-filename=initramfs-with-modules-%%KV%%.img initramfs
	fi

	if [[ $1 == existing ]]
	then
		genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --no-clean --no-mrproper --no-splash --no-ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/usr/src/linux/.config all && genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --no-clean --no-mrproper --no-splash --ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/usr/src/linux/.config --initramfs-filename=initramfs-with-modules-%%KV%%.img initramfs
	fi

	if [[ $1 == select ]]
	then
		IFS=$'\n' array=($(ls --full-time --sort=time /etc/kernels/*))
		for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo [$i] ${array[$i]}; done
		read -p $'\nSelection: ' select
		if [[ $select =~ ^[0-9]+$ ]] && [ $select -ge 0 -a $select -lt ${#array[@]} ]
		then
			if [[ -e /usr/src/linux/.config  ]]; then mv --verbose --force /usr/src/linux/.config /usr/src/linux/.config--$(date +%Y-%m-%d--%H-%M-%S).bak; fi
			genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --mrproper --no-splash --no-ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=$(echo ${array[$select]} | awk '{print $9}') all && genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --no-clean --no-mrproper --no-splash --ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/usr/src/linux/.config --initramfs-filename=initramfs-with-modules-%%KV%%.img initramfs
		else
			echo -e "\nInvalid selection.\n"
		fi
	fi

	if [[ $1 == default ]]
	then
		if [[ -e /usr/src/linux/.config  ]]; then mv --verbose --force /usr/src/linux/.config /usr/src/linux/.config--$(date +%Y-%m-%d--%H-%M-%S).bak; fi
		genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --mrproper --no-splash --no-ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/usr/share/genkernel/arch/x86_64/generated-config all && genkernel --color --menuconfig --save-config --no-hyperv --no-virtio --no-clean --no-mrproper --no-splash --ramdisk-modules --no-nice --bcache --no-lvm --mdadm --mdadm-config=/etc/mdadm.conf --no-microcode-initramfs --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --no-multipath --no-iscsi --ssh --ssh-authorized-keys-file=/root/.ssh/authorized_keys --ssh-host-keys=runtime --no-bootloader --luks --gpg --b2sum --busybox --no-unionfs --no-netboot --no-integrated-initramfs --no-wrap-initrd --compress-initramfs --compress-initramfs-type=best --kernel-config=/usr/src/linux/.config --initramfs-filename=initramfs-with-modules-%%KV%%.img initramfs
	fi
else
	echo -e "\nBuild kernel using genkernel.\n\n\tbuildkernel.sh <argument>\n\n\tarchlinux\tPull latest archlinux lts config file and build kernel.\n\texisting\tUse existing config file (/usr/src/linux/.config) and build kernel.\n\tselect\t\tSelect from available kernel config files (/etc/kernels/) and build kernel.\n\tdefault\t\tUse default genkernel config file (/usr/share/genkernel/arch/x86_64/generated-config) and build kernel.\n\nNote: Each option builds two initramfs; one without modules and another with modules.\n"
fi
