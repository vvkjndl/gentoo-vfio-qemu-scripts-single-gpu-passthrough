#!/bin/bash

backdir=/home/user/
backfile=backup-kvm
backinclude=/root/scripts/backup-files-include
backexclude=/root/scripts/backup-files-exclude

# tar backup with gzip and checksum generation
tar --create --dereference --verbose --exclude-from=$backexclude --file - --files-from=$backinclude | pigz --best > $backdir/$backfile.tar.gz
echo "$(sha512sum $backdir/$backfile.tar.gz | cut -d ' ' -f1)  $backfile.tar.gz" > $backdir/$backfile.tar.gz.sha512sum
