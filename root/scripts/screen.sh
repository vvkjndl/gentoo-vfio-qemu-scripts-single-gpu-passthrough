#!/bin/bash

if [[ -z $1 ]]
then
	echo -e "\nExecute command in screen session and connect to it.\n\n\tscreen.sh <script.sh>/<command>\n\nLogs will be saved in /home/logs/screen-?.log\n"
	exit 0
fi
session=$(echo "$1" | awk --field-separator='/' '{print $NF}' | sed 's/.sh//g')
cat /dev/null > /home/logs/screen-$session.log
screen -dmS $session -L -Logfile /home/logs/screen-$session.log bash -c 'starttime=$(date | sed "s/\ \ /\ /g"); "$0" "$@"; endtime=$(date | sed "s/\ \ /\ /g"); echo -e "\nSession started at $starttime"; echo -e "Session finished at $endtime\n"; read -s -n 1 -p "Press any key to exit ..."; echo -e "\n\n"' $@
sleep 1; screen -r $session
