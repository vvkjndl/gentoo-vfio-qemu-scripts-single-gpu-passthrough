#!/bin/bash

workdir=/root/scripts

if [[ -z $1 ]]
then
	echo -e "\nExecute specified VM script using screen and bash.\n\n\trunvm.sh <script.sh>\n"
	exit 0
fi

if [[ -e $1 ]]
then
	mkdir --parents --verbose /home/logs
	vmname=$(echo "$1" | awk --field-separator='/' '{print $NF}' | sed 's/.sh//g')
	screen -dmS $vmname -L -Logfile /home/logs/$vmname-$(date +%Y-%m-%d--%H-%M-%S).log /bin/bash $workdir/$vmname.sh
	/bin/bash $workdir/qemu-isolate-cpus.sh
else
	echo -e "Specified script doesn't exist."
	exit 1
fi
