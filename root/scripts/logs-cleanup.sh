#!/bin/bash

# delete logs older that 31*24 hours

logsdir="/home/logs"

IFS=$'\n' array=($(find $logsdir -type f -regex ".*\.log$" ! -regex ".*deleted\-files\.log$" -mtime +31))
(for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo [$(($i+1))] ${array[$i]}; done) | tee $logsdir/deleted-files.log
printf "\n" >> $logsdir/deleted-files.log

read -p $'\nProceed with removal? (yes/no): ' select

if [[ $select == yes  ]]
then
	(for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo ${array[$i]}; done) | xargs --replace={} rm --force --verbose {} | tee --append $logsdir/deleted-files.log
	exit 0
else
	echo "Aborted!"
	exit 1
fi
